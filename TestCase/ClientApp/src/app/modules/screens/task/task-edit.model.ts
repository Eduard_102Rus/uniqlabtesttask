export class TaskEdit {
  constructor(id:number ,public name: string, public description: string, public isDone: boolean, public deadLine: Date) {

  }
}
