export class CreateTask {
  constructor(public name: string, public description: string, public isDone: boolean, public deadLine: Date) {
    
  }
}
