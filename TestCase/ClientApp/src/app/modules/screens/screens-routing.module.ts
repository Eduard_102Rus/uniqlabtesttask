import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';


const routes: Routes = [

  {
    path: 'tasks',
    loadChildren:'./task/task.module#TaskModule'
  },
  {
    path: '', component: HomeComponent, pathMatch:'full'
  }

]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class ScreensRoutingModule { }
